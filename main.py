from flask import Flask, render_template, send_file, redirect


app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')

@app.route("/favicon.ico")
def favicon():
    return send_file("static/favicon.ico")

@app.route('/changelog')
def changelog():
    return render_template('changelog.html')

@app.route('/info')
def info():
    return render_template('info.html')

def send_ok():
    return {"ping": "pong"}

if __name__ == '__main__':
    app.run(debug=True)